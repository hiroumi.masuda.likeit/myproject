package beans;

import java.io.Serializable;
import java.util.Date;

public class UserDataBeans implements Serializable {
	private int id;
	private String name;
	private String loginId;
	private String password;
	private Date birthDate;
	private Date createDate;

	public UserDataBeans(int id2, String loginId1, String name2, Date birthDate2, String password1,
			Date createDate2) {
		this.id=id2;
		this.loginId=loginId1;
		this.name=name2;
		this.birthDate=birthDate2;
		this.password=password1;
		this.createDate=createDate2;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	}
