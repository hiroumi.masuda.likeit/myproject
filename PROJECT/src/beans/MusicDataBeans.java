package beans;

import java.io.Serializable;
import java.util.Date;

public class MusicDataBeans implements Serializable {
	private int id;
	private String mName;
	private String aName;
	private int genre;
	private String img;
	private String music;
	private Date createDate;
	public MusicDataBeans(int id2, String mName2, String aName2, int genre1, String img2, String music1,Date createDate2) {
		this.id=id2;
		this.mName=mName2;
		this.aName=aName2;
		this.genre=genre1;
		this.img=img2;
		this.music=music1;
		this.createDate=createDate2;

	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getmName() {
		return mName;
	}
	public void setmName(String mName) {
		this.mName = mName;
	}
	public String getaName() {
		return aName;
	}
	public void setaName(String aName) {
		this.aName = aName;
	}
	public int getGenre() {
		return genre;
	}
	public void setGenre(int genre) {
		this.genre = genre;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getMusic() {
		return music;
	}
	public void setMusic(String music) {
		this.music = music;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
