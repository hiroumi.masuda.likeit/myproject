package beans;

import java.io.Serializable;
import java.util.Date;

public class ReviewDateBeans implements Serializable {
	private int id;
	private int rating;
	private String review;
	private int musicId;
	private Date createDate;
	private int userid;
	private String img;
	private String m_name;
	private String a_name;
	private String name;
	private double point;

	private int rank;

	public ReviewDateBeans(int id1, int rating2, String review2, int music_id, int user_id, java.sql.Date createDate2,
			String img, String m_name1, String a_name1 ,String name) {
		this.id=id1;
		this.rating=rating2;
		this.review=review2;
		this.musicId=music_id;
		this.userid=user_id;
		this.createDate=createDate2;
		this.img=img;
		this.m_name=m_name1;
		this.a_name=a_name1;
		this.name=name;

	}

	public ReviewDateBeans(int id1, int rating2, String review2, int music_id, int user_id, java.sql.Date createDate2,
			String img2, String m_name1, String a_name1) {
		this.id=id1;
		this.rating=rating2;
		this.review=review2;
		this.musicId=music_id;
		this.userid=user_id;
		this.createDate=createDate2;
		this.img=img2;
		this.m_name=m_name1;
		this.a_name=a_name1;
	}

	public ReviewDateBeans(int id1, int rating2, String review2, int music_id, int user_id, java.sql.Date createDate2,
			String name2) {
		this.id=id1;
		this.rating=rating2;
		this.review=review2;
		this.musicId=music_id;
		this.userid=user_id;
		this.createDate=createDate2;
		this.name=name2;

	}

	public ReviewDateBeans(double point) {
		this.point=point;
	}

	public ReviewDateBeans(int music_id, double point2, String m_name1, String a_name1,int rank) {
		this.musicId=music_id;
		this.point=point2;
		this.m_name=m_name1;
		this.a_name=a_name1;
		this.rank=rank;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public String getReview() {
		return review;
	}
	public void setReview(String review) {
		this.review = review;
	}
	public int getMusicId() {
		return musicId;
	}
	public void setMusicId(int musicId) {
		this.musicId = musicId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getM_name() {
		return m_name;
	}
	public void setM_name(String m_name) {
		this.m_name = m_name;
	}
	public String getA_name() {
		return a_name;
	}
	public void setA_name(String a_name) {
		this.a_name = a_name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public double getPoint() {
		return this.point / 5 * 100;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}
}
