package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ReviewDateBeans;

public class ReviewDAO {
	public void InserIntoCreateReview(int rate,String review,String music_id,String user_id) {
		// TODO 自動生成されたメソッド・スタブ
		Connection conn = null;
		new ArrayList<ReviewDateBeans>();

		try {

			conn = DBManager.getConnection();

			String sql = "insert into review (rating,review,music_id,user_id,create_date)values(?,?,?,?,now())";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setInt(1, rate);
			pStmt.setString(2, review);
			pStmt.setString(3, music_id);
			pStmt.setString(4, user_id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
		return;
	}
	public List<ReviewDateBeans> findById(int id) {
		Connection conn = null;
		List<ReviewDateBeans> reviewList = new ArrayList<ReviewDateBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM review inner join music on review.music_id=music.id where user_id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();


			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id1 = rs.getInt("id");
				int rating2 = rs.getInt("rating");
				String review2 = rs.getString("review");
				int music_id = rs.getInt("music_id");
				int user_id = rs.getInt("user_id");
				Date createDate = rs.getDate("create_date");
				String img=rs.getString("img");
				String m_name1=rs.getString("m_name");
				String a_name1=rs.getString("a_name");
				ReviewDateBeans review = new ReviewDateBeans(id1, rating2, review2, music_id, user_id, createDate,img,m_name1,a_name1);

				reviewList.add(review);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return reviewList;
	}
	public List<ReviewDateBeans> findBystrId(String id) {
		Connection conn = null;
		List<ReviewDateBeans> reviewList = new ArrayList<ReviewDateBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM review inner join user on review.user_id=user.id where music_id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();


			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id1 = rs.getInt("id");
				int rating2 = rs.getInt("rating");
				String review2 = rs.getString("review");
				int music_id = rs.getInt("music_id");
				int user_id = rs.getInt("user_id");
				Date createDate = rs.getDate("create_date");
				String name2=rs.getString("name");
				ReviewDateBeans review = new ReviewDateBeans(id1, rating2, review2, music_id, user_id, createDate,name2);

				reviewList.add(review);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return reviewList;
	}
	public ReviewDateBeans findByIdPoint(String id) {
		Connection conn = null;


		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT review.music_id, avg(review.rating) as point FROM review inner join music on review.music_id=music.id where music.id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();


			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				double point = rs.getDouble("point");

				return new ReviewDateBeans(point);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;

	}
	public List<ReviewDateBeans> findAllRank() {
		Connection conn = null;
		List<ReviewDateBeans> reviewList = new ArrayList<ReviewDateBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT review.music_id, avg(review.rating)as point, music.`m_name`, music.`a_name`  FROM review inner join music on review.music_id=music.id group by review.music_id ORDER by point desc;";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();


			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			int rank =1;
			while (rs.next()) {

				int music_id = rs.getInt("music_id");
				double point = rs.getInt("point");
				String m_name1=rs.getString("m_name");
				String a_name1=rs.getString("a_name");

				ReviewDateBeans review = new ReviewDateBeans(music_id,point,m_name1,a_name1,rank);
				rank++;

				reviewList.add(review);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return reviewList;
	}
	public void delete(String id) {
		// TODO 自動生成されたメソッド・スタブ
		Connection conn = null;


		try {

			conn = DBManager.getConnection();

			String sql = "delete from review where id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
		return;
	}
}
