package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.UserDataBeans;

public class UserDAO {
	public void InserIntoCreateInfo(String loginId, String password, String name, String date) {
		// TODO 自動生成されたメソッド・スタブ
		Connection conn = null;
		new ArrayList<UserDataBeans>();

		try {

			conn = DBManager.getConnection();
			String seacretPass = Password.seacret(password);
			String sql = "insert into user (login_id,login_password,name,birth_date,create_date)values(?,?,?,?,now())";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, loginId);
			pStmt.setString(2, seacretPass);
			pStmt.setString(3, name);
			pStmt.setString(4, date);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
		return;
	}

public UserDataBeans findByLoginInfo(String loginId, String password) {
	Connection conn = null;
	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM user WHERE login_id = ? and login_password = ?";
		String seacretPass = Password.seacret(password);
		// SELECTを実行し、結果表を取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, loginId);
		pStmt.setString(2, seacretPass);
		ResultSet rs = pStmt.executeQuery();

		// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
		if (!rs.next()) {
			return null;
		}

		// 必要なデータのみインスタンスのフィールドに追加
		int id2 = rs.getInt("id");
		String loginId1 = rs.getString("login_id");
		String name = rs.getString("name");
		Date birthDate = rs.getDate("birth_date");
		String password1 = rs.getString("login_password");
		Date createDate = rs.getDate("create_date");

		return new UserDataBeans(id2, loginId1, name, birthDate, password1, createDate);


	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}



public List<UserDataBeans> findAll() {
	Connection conn = null;
	List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		// TODO: 未実装：管理者以外を取得するようSQLを変更する
		String sql = "SELECT * FROM user where id!=1";

		// SELECTを実行し、結果表を取得
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);

		// 結果表に格納されたレコードの内容を
		// Userインスタンスに設定し、ArrayListインスタンスに追加
		while (rs.next()) {
			int id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("login_password");
			Date createDate = rs.getDate("create_date");
			UserDataBeans user = new UserDataBeans(id, loginId, name, birthDate, password, createDate);

			userList.add(user);
		}
	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	return userList;
}
public UserDataBeans findById(String id) {
	Connection conn = null;
	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "select * from user where id=?";

		// SELECTを実行し、結果表を取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, id);
		ResultSet rs = pStmt.executeQuery();

		// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
		if (!rs.next()) {
			return null;
		}

		// 必要なデータのみインスタンスのフィールドに追加
		int id2 = rs.getInt("id");
		String loginId1 = rs.getString("login_id");
		String name = rs.getString("name");
		Date birthDate = rs.getDate("birth_date");
		String password1 = rs.getString("login_password");
		Date createDate = rs.getDate("create_date");

		return new UserDataBeans(id2, loginId1, name, birthDate, password1, createDate);

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}
public void update(int id, String password, String name, String date) {
	// TODO 自動生成されたメソッド・スタブ
	Connection conn = null;

	try {

		conn = DBManager.getConnection();

		String sql = "update user set login_password=?,name=?,birth_date=? where id=?";

		PreparedStatement pStmt = conn.prepareStatement(sql);
		String seacretPass = Password.seacret(password);


		pStmt.setString(1, seacretPass);
		pStmt.setString(2, name);
		pStmt.setString(3, date);
		pStmt.setInt(4, id);

		pStmt.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
		return;
	} finally {

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return;
			}
		}
	}
	return;
}

public void update2(int id, String name, String date) {
	// TODO 自動生成されたメソッド・スタブ
	Connection conn = null;


	try {

		conn = DBManager.getConnection();

		String sql = "update user set name=?,birth_date=? where id=?";

		PreparedStatement pStmt = conn.prepareStatement(sql);

		pStmt.setString(1, name);
		pStmt.setString(2, date);
		pStmt.setInt(3, id);

		pStmt.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
		return;
	} finally {

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return;
			}
		}
	}
	return;
}
public void delete(int id) {
	// TODO 自動生成されたメソッド・スタブ
	Connection conn = null;


	try {

		conn = DBManager.getConnection();

		String sql = "delete from user where id=?";

		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setInt(1, id);

		pStmt.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
		return;
	} finally {

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return;
			}
		}
	}
	return;
}

}