package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.MusicDataBeans;

public class MusicDAO {
	public void InserIntoCreateMusic(String m_name, String a_name, String genre,String img,String music) {
		// TODO 自動生成されたメソッド・スタブ
		Connection conn = null;
		new ArrayList<MusicDataBeans>();

		try {

			conn = DBManager.getConnection();

			String sql = "insert into music (m_name,a_name,genre,img,music,create_date)values(?,?,?,?,?,now())";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, m_name);
			pStmt.setString(2, a_name);
			pStmt.setString(3, genre);
			pStmt.setString(4, img);
			pStmt.setString(5, music);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
		return;
	}
	public List<MusicDataBeans> find(String serchWord,String genre) {
		Connection conn = null;
		List<MusicDataBeans> musicList = new ArrayList<MusicDataBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();


			// SELECT文を準備
			String sql = "select * from music where (a_name like '%"+ serchWord +"%' || m_name  like '%" + serchWord + "%')";

			if(!(genre==null)) {
				sql += " AND genre = '" + genre + "'";
			}

			System.out.println(sql);

			Statement stmt = conn.createStatement();
	        ResultSet rs = stmt.executeQuery(sql);


	        while (rs.next()) {
				int id = rs.getInt("id");
				String MName = rs.getString("m_name");
				String AName = rs.getString("a_name");
				int genre1= rs.getInt("genre");
				String img = rs.getString("img");
				String music1 = rs.getString("music");
				Date createDate = rs.getDate("create_date");
				MusicDataBeans AllMusic = new MusicDataBeans(id, MName, AName, genre1, img, music1, createDate);

				musicList.add(AllMusic);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return musicList;

	}
	public MusicDataBeans findById(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "select * from music where id=?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int id1 = rs.getInt("id");
			String MName = rs.getString("m_name");
			String AName = rs.getString("a_name");
			int genre1= rs.getInt("genre");
			String img = rs.getString("img");
			String music1 = rs.getString("music");
			Date createDate = rs.getDate("create_date");

			return new MusicDataBeans(id1, MName, AName, genre1, img, music1, createDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

}