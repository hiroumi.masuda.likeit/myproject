package project;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.MusicDataBeans;
import dao.MusicDAO;

/**
 * Servlet implementation class MusicListServlet
 */
@WebServlet("/MusicListServlet")
public class MusicListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MusicListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {

			String searchWord = request.getParameter("searchWord");
			String genre = request.getParameter("genre");


			MusicDAO musicdao = new MusicDAO ();
			List<MusicDataBeans> musicList= musicdao.find(searchWord, genre);
			request.setAttribute("musicList",musicList);

			if (musicList.size()==0) {

				request.setAttribute("errMsg", "まだその楽曲は登録されていません");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/musicList.jsp");
				dispatcher.forward(request, response);
				return;
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/musicList.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
