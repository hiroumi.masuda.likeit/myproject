package project;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import beans.GenreDataBeans;
import dao.GenreDao;
import dao.MusicDAO;

/**
 * Servlet implementation class MusicCreateServlet
 */
@WebServlet("/MusicCreateServlet")
@MultipartConfig(location="/Users/masudahiroumi/Documents/Git/myproject/PROJECT/WebContent", maxFileSize=104857600)
public class MusicCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MusicCreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ArrayList<GenreDataBeans> dMDBList = null;
		try {
			dMDBList = GenreDao.getAllGenreDataBeans();
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		request.setAttribute("dmdbList", dMDBList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/musicCreate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String  m_name= request.getParameter("m_name");
		String a_name = request.getParameter("a_name");
		String id = request.getParameter("genre");


		Part img = request.getPart("img");
		String imgName = this.getFileName(img);
		img.write("img/" + imgName);


		Part music = request.getPart("music");
		String musicName = this.getFileName(music);
		music.write("music/" + musicName);




		MusicDAO musicdao = new MusicDAO();

		musicdao.InserIntoCreateMusic(m_name,a_name,id,imgName,musicName);
		response.sendRedirect("MusicListServlet");

	}

    private String getFileName(Part part) {
        String name = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
        }
        return name;
    }

}
