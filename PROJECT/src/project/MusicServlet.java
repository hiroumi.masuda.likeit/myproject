package project;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.MusicDataBeans;
import beans.ReviewDateBeans;
import dao.MusicDAO;
import dao.ReviewDAO;

/**
 * Servlet implementation class MusicServlet
 */
@WebServlet("/MusicServlet")
public class MusicServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MusicServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		System.out.println(id);

		MusicDAO musicdao = new MusicDAO();
		MusicDataBeans music = musicdao.findById(id);
		request.setAttribute("music", music);

		ReviewDAO reviewdao =new ReviewDAO();
		List<ReviewDateBeans> review =reviewdao.findBystrId(id);
		request.setAttribute("review", review);


		ReviewDateBeans point =reviewdao.findByIdPoint(id);
		request.setAttribute("point", point.getPoint());





		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/music.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
