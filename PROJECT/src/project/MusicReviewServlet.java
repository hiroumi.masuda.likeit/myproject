package project;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.MusicDataBeans;
import dao.MusicDAO;
import dao.ReviewDAO;

/**
 * Servlet implementation class MusicReviewServlet
 */
@WebServlet("/MusicReviewServlet")
public class MusicReviewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MusicReviewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		System.out.println(id);
		MusicDAO musicdao = new MusicDAO();
		MusicDataBeans music = musicdao.findById(id);
		request.setAttribute("music", music);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/musicReview.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String rate = request.getParameter("rate");
		int i = Integer.parseInt(rate);
		String review = request.getParameter("review");
		String music_id=request.getParameter("music_id");
		String user_id=request.getParameter("user_id");

		ReviewDAO reviewdao=new ReviewDAO();
		reviewdao.InserIntoCreateReview(i, review, music_id, user_id);
		String url = "MusicServlet?id=" + music_id;
		response.sendRedirect(url);

	}

}
