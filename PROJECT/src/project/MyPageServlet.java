package project;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ReviewDateBeans;
import beans.UserDataBeans;
import dao.ReviewDAO;

/**
 * Servlet implementation class MyPageServlet
 */
@WebServlet("/MyPageServlet")
public class MyPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyPageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserDataBeans u =(UserDataBeans)session.getAttribute("userInfo");

		ReviewDAO reviewdao=new ReviewDAO();
		List<ReviewDateBeans> reviewList=reviewdao.findById(u.getId());
		request.setAttribute("reviewList", reviewList);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/myPage.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ReviewDAO reviewdao=new ReviewDAO();
		List<ReviewDateBeans> rank=reviewdao.findAllRank();
		request.setAttribute("rank",rank);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/rank.jsp");
		dispatcher.forward(request, response);
	}
}
