<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>musicList</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">


</head>

<body>



	<header>
		<nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
			<ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="index.html">Music List</a></li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link" href="#">${userInfo.name}さん</a>
				</li>
				<li class="nav-item"><a class="btn btn-primary"
					href="LoginServlet">ログアウト</a></li>
			</ul>
		</nav>
	</header>

	<div class="container">



		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

		<c:forEach var="MusicDataBeans" items="${musicList}">

			<div class="card-deck">
				<div class="card">
					<img src="img/${MusicDataBeans.img}"
						class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">
							${MusicDataBeans.mName}<br> ${MusicDataBeans.aName}
						</h5>
						<a href="MusicServlet?id=${MusicDataBeans.id}">
							<button type="button" class="btn btn-secondary">
								楽曲の詳細を見る</button>
						</a>
					</div>
					<div class="card-footer">
						<small class="text-muted">投稿日
						<br> ${MusicDataBeans.createDate}</small>
					</div>
				</div>
			</div>
		</c:forEach>




		<div class=user-button>
			<a href="MusicCreateServlet">
				<button type="button" class="btn btn-secondary">
					楽曲を追加する場合はこちら</button>
			</a>
		</div>
	</div>
</body>
</html>
