<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>music</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/common.css" rel="stylesheet">


</head>

<body>

    <header>
        <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
            <ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.html">Review</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="nav-link" href="#">ログインユーザの名前を表示</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="login.html">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>
<form action="MusicReviewServlet" method="post">
    <div class="container">

        <div class="main">
          <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <img src="img/${music.img}" class="card-img-top" alt="...">
            </div>

            <div class="col-sm-9">
                <p>${music.mName} </p>
                <p>${music.aName}</p>


                <div class="star-rating">
                    <div class="star-rating-front" style="width: 50%">★★★★★</div>
                    <div class="star-rating-back">★★★★★</div>
                </div>
            </div>
              </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="passwordConf" class="col-sm-2 col-form-label mt-3" style="margin-right:18px">評価</label>
                <select name="rate" class="form-control mt-3" style="width: 500px">
                    <option value="5">⭐️⭐️⭐️⭐️⭐️</option>
                    <option value="4">⭐️⭐️⭐️⭐️</option>
                    <option value="3">⭐️⭐️⭐️</option>
                    <option value="2">⭐️⭐️</option>
                    <option value="1">⭐️</option>
                </select>
        </div>

        <div class="form-group row">
            <label for="passwordConf" class="col-sm-2 col-form-label mt-3">　レビュー</label>
            <div class="col-sm-10">
                <input type="text" name="review" class="form-control mt-3">
            </div>
        </div>

        <div class="under">
            <div class="submit-button-area" style="width: 500px;">
                <button type="submit" value="検索" class="btn btn-success btn-block">評価する</button>
            </div>
        </div>
    </div>
    		<input type="hidden" name="music_id" value="${music.id}">
    		<input type="hidden" name="user_id" value="${userInfo.id}">

    </form>
</body></html>
