<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Create</title>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <header>
        <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
            <ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
                <li class="nav-item active">
                    <a>ユーザ管理システム</a>
                </li>
            </ul>
        </nav>
    </header>
    <div class="container">

    <c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>


    <form action="UserCreateServlet" class="form-horizontal" method="post">
      <div class="form-group row">
        <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="loginId">
        </div>
      </div>

      <div class="form-group row">
        <label for="password" class="col-sm-2 col-form-label">パスワード</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" name="password">
        </div>
      </div>

      <div class="form-group row">
        <label for="password2" class="col-sm-2 col-form-label">パスワード(確認)</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" name="password2">
        </div>
      </div>

      <div class="form-group row">
        <label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="name">
        </div>
      </div>

      <div class="form-group row">
        <label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
        <div class="col-sm-10">
          <input type="date" class="form-control" name="date">
        </div>
      </div>

      <div class="submit-button-area">
        <button type="submit" value="検索" class="btn btn-primary btn-lg btn-block">登録</button>
      </div>

      <div class="col-xs-4">
        <a href="LoginServlet">戻る</a>
      </div>

    </form>
    </div>
</body>
</html>