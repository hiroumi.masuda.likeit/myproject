<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- header -->
<hgroup class="heading">
<h1 class="major">Login Form </h1>
</hgroup>
  <link href="css/login.css" rel="stylesheet">
  <html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン画面</title>
<link href="css/login.css" rel="stylesheet">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/login.css" rel="stylesheet">

</head>

<body>
<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
<!-- form starts here -->
<form class="sign-up" action="LoginServlet" method="post">
    <h1 class="sign-up-title">ようこそ</h1>
    <input type="text" name="loginId" class="sign-up-input" placeholder="ユーザー名" autofocus>
    <input type="password" name="password" class="sign-up-input" placeholder="パスワード">
    <input type="submit"  value="ログイン" class="sign-up-button">
  </form>


<div style="text-align: center;">
    <a href="UserCreateServlet">
  <button type="button" class="btn"　autofocus="true">新規登録</button>
    </a>
</div>
</body>
</html>