<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MyPage</title>
<title>ユーザー登録画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

</head>

<body>


	<div class="input-field">
		<header>
			<nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
				<ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
					<li class="nav-item active"><a class="nav-link">My Page</a></li>
				</ul>
				<ul class="navbar-nav flex-row">
					<li class="nav-item"><a class="nav-link" href="#">${userInfo.name}さん</a>
					</li>
					<li class="nav-item"><a
						href="UserUpdateServlet?id=${userInfo.id}">
							<button type="button" class="btn btn-secondary">ユーザー情報を変更する場合はこちら</button>
					</a></li>
					<li class="nav-item"><a
						href="UserDeleteServlet?id=${userInfo.id}">
							<button type="button" class="btn btn-danger">退会する場合はこちら</button>
					</a></li>
					<li class="nav-item"><a class="btn btn-primary"
						href="LoginServlet">ログアウト</a></li>
				</ul>
			</nav>
		</header>
	</div>
	<div class="container">
		<div class="l-search">
			<form action="MusicListServlet" method="get" class="l-search__form">
				<fieldset class="l-search-field">
					<h2>曲を検索</h2>
					<input autocomplete="off" id="q" name="searchWord"
						placeholder="曲名・アーティスト名で検索" type="text"
						class="l-search-field__query">
				</fieldset>
				<div class="l-search__types">
					<div class="p-search-type">
						<div class="c-input-radio__label c-input-radio">
							<input id="search-type-content" name="genre" type="radio"
								value="1"
								class="c-input-radio__check c-input-radio__check--search"><label
								for="search-type-content">ロック</label>
						</div>
					</div>
					<div class="p-search-type">
						<div class="c-input-radio__label c-input-radio">
							<input id="search-type-people" name="genre" type="radio"
								value="2"
								class="c-input-radio__check c-input-radio__check--search"><label
								for="search-type-people">ジャズ</label>
						</div>
					</div>
					<div class="p-search-type">
						<div class="c-input-radio__label c-input-radio">
							<input id="search-type-user" name="genre" type="radio" value="3"
								class="c-input-radio__check c-input-radio__check--search"><label
								for="search-type-user">ファンク</label>
						</div>
					</div>
					<div class="p-search-type">
						<div class="c-input-radio__label c-input-radio">
							<input id="search-type-user" name="genre" type="radio" value="4"
								class="c-input-radio__check c-input-radio__check--search"><label
								for="search-type-user">ポップ</label>
						</div>
					</div>
					<div class="p-search-type">
						<div class="c-input-radio__label c-input-radio">
							<input id="search-type-user" name="genre" type="radio" value="5"
								class="c-input-radio__check c-input-radio__check--search"><label
								for="search-type-user">クラシック</label>
						</div>
					</div>
					<div class="p-search-type">
						<div class="c-input-radio__label c-input-radio">
							<input id="search-type-user" name="genre" type="radio" value="6"
								class="c-input-radio__check c-input-radio__check--search"><label
								for="search-type-user">クラブ</label>
						</div>
					</div>
					<button type="submit" class="c-button-1">検索</button>
					<div class="container" style="margin-top: 20px"></div>
				</div>

			</form>
			<form action="MyPageServlet" method="post" class="l-search__form">
				<button class="btn-gradation">ランキングを見る!</button>
			</form>
		</div>



		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item active" aria-current="page">今までのレビュー</li>
			</ol>
		</nav>

		<c:forEach var="ReviewDataBeans" items="${reviewList}">
			<div class="card-deck">
				<div class="card">
					<img src="img/${ReviewDataBeans.img}" class="card-img-top"
						alt="...">
					<div class="card-body">
						<h5 class="card-title">${ReviewDataBeans.rating}/
							${ReviewDataBeans.m_name}/${ReviewDataBeans.a_name}</h5>
						<a href="MusicServlet?id=${ReviewDataBeans.musicId}">
							<button type="button" class="btn btn-secondary">
								楽曲の詳細を見る</button>
						</a>
						<p class="card-text">${ReviewDataBeans.review}</p>
					</div>
					<div class="card-footer">
						<small class="text-muted">${ReviewDataBeans.createDate}</small>
					</div>
				</div>

			</div>

		</c:forEach>
</body>
</html>