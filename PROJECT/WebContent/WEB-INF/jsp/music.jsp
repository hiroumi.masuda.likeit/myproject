<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>music</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/common.css" rel="stylesheet">


</head>

<body>
    <header>
        <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
            <ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="MyPageServlet">Music</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="nav-link" href="#">ログインユーザの名前を表示</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="LoginServlet">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>

    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <img src="img/${music.img}" class="card-img-top" alt="...">
            </div>

            <div class="col-sm-9">
                <h1>${music.mName}</h1>
                <p>${music.aName}</p>


                <audio src="music/${music.music}" controls>
                    <p>曲</p>
                </audio>

                <div class="star-rating">
                    <div class="star-rating-front" style="width: ${point}%">★★★★★</div>
                    <div class="star-rating-back">★★★★★</div>
                </div>


                <a type="button" class="btn btn-outline-info" href="MusicReviewServlet?id=${music.id}" style="margin-top: 50px">評価する</a>


            </div>
        </div>




        <h3 class="c-heading-5 c-heading-5--with-icon" style="background-color: darkgray; margin-top: 100px"><img alt="" class="c-heading-5__icon" src="https://d2ueuvlup6lbue.cloudfront.net/assets/pc/component/ico_reviews-b411320aa9d006ccf1dd4ff8f22f4962ef0d0677b0a4d44b01a90ca9e8a0bbb6.svg" width="23" height="18">投稿された感想・評価</h3>
        <c:forEach var="ReviewDataBeans" items="${review}">
        <div class="list">
            <div class="card bg-light mb-3" style="max-width: 100rem;">
                <div class="card-header">${ReviewDataBeans.rating}</div>
                <div class="card-body">
                    <h5 class="card-title">${ReviewDataBeans.name}</h5>
                    <p class="card-text">${ReviewDataBeans.review}</p>
                    <c:if test="${userInfo.id==2}">
                    <a href="deleteServlet?id=${ReviewDataBeans.id}">
                    <button type="button" class="btn btn-outline-danger">削除</button>
                    </a>
                    </c:if>
                </div>

            </div>

</div>

</c:forEach>


</body></html>