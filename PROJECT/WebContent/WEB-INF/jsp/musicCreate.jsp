<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Create</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>
	<header>
		<nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
			<ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="index.html">Music Create</a></li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link" href="#">ログインユーザの名前を表示</a>
				</li>
				<li class="nav-item"><a class="btn btn-primary"
					href="LoginServlet">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<div class="container">


		<form action="MusicCreateServlet" method="post" class="form-horizontal" enctype="multipart/form-data">
			<div class="form-group row">
				<label for="loginId" class="col-sm-2 col-form-label">曲名</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="m_name">
				</div>
			</div>

			<div class="form-group row">
				<label for="password" class="col-sm-2 col-form-label">アーティスト名</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="a_name">
				</div>
			</div>

			<div class="l-search__types">
				<c:forEach var="dmdb" items="${dmdbList}">
					<div class="p-search-type">
						<div class="c-input-radio__label c-input-radio">
							<input id="search-type-content" name="genre" type="radio" value="${dmdb.id}"
								class="c-input-radio__check c-input-radio__check--search"><label
								for="search-type-content">
								${dmdb.name}
							</label>
						</div>
					</div>
														</c:forEach>


				<label for="search-type-content">音楽を添付してください</label><input
					type="file" id="avatar" name="music" accept="audio/mpeg">
				<div>
					<label for="search-type-content">画像を添付してください</label><input
						type="file" id="avatar" name="img" accept="image/jpeg">
				</div>

			</div>


			<div class="submit-button-area">

				<button type="submit" value="検索"
					class="btn btn-primary btn-lg btn-block">登録</button>
			</div>
		</form>
		<div class="col-xs-4">
			<a href="MusicListServlet">戻る</a>
		</div>




	</div>
</body>
</html>