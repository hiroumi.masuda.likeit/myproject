<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MyPage</title>
<title>ユーザー登録画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">
</head>

<body>
	<header>
		<nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
			<ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="index.html">Ranking</a></li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link" href="#">${userInfo.name}さん</a>
				</li>
				<li class="nav-item"><a class="btn btn-primary"
					href="LoginServlet">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<div class="container">
		<h1>ランキング</h1>
		<div class="table-responsive" style="margin-top: 100px;">
			<table class="table table-striped">
				<thead class="thead-dark">
					<tr>
						<th>順位</th>
						<th>曲名</th>
						<th>アーティスト</th>
						<th>スコア</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="ReviewDataBeans" items="${rank}">
						<c:choose>
							<c:when test="${ReviewDataBeans.rank ==1}">
								<tr style="background-color: #ffd700;">
							</c:when>
							<c:when test="${ReviewDataBeans.rank ==2}">
								<tr style="background-color: #cccccc;">
							</c:when>
							<c:when test="${ReviewDataBeans.rank ==3}">
								<tr style="background-color: #cd853f;">
							</c:when>
							<c:otherwise>
								<tr>
							</c:otherwise>
						</c:choose>



						<td>${ReviewDataBeans.rank}位</td>
						<td>${ReviewDataBeans.m_name}</td>
						<td>${ReviewDataBeans.a_name}</td>
						<td>${ReviewDataBeans.point}</td>
						<td><a type="button" class="btn btn-outline-info"
							href="MusicServlet?id=${ReviewDataBeans.musicId}">詳細</a></td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>


		<div class="col-xs-4">
			<a href="MyPageServlet">戻る</a>
		</div>
	</div>
</body>
</html>